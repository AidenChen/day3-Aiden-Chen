## Objective
- Code View.Is to let others see my code and help me improve my code quality,find the potential
  issue and learn from others.
- Java stream Api.This part helps up code more concise.
- OOP.Learned three major characteristics of object-orientation like encapsulation,inheritance
  and polymorphism.
- Practice OO code.

## Reflective
- feel fresh
- great
- normal mind


## Interpretive
- Because the Code view hasn't heard of it before, it feels fresh.It helps us improve the
  quality of the code.
- Because I wasn't particularly familiar with the Java Stream API before.
- The object-oriented part is relatively familiar, so it is a normal state of mind.


## Decisional
Practice the Java Stream API more and supplement the parts that the teacher did not cover in
detail, such as peek(), allMatch() and reduce() etc...

