package ooss;

public interface Observer {

    void response(Klass klass);
}
