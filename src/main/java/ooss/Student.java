package ooss;

public class Student extends Person implements Observer{

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public void response(Klass klass) {
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.%n", getName(), klass.getNumber(), klass.getLeader().getName());
    }

    @Override
    public String introduce() {
        return this.klass == null ? String.format("%s I am a student.", super.introduce())
                                  : classIntroduction();
    }

    public String classIntroduction() {
        return this.equals(this.klass.getLeader()) ? classLeaderIntroduction()
                                                   : String.format("%s I am a student. I am in class %d.", super.introduce(), this.klass.getNumber());
    }

    public String classLeaderIntroduction() {
        return String.format("%s I am a student. I am the leader of class %d.", super.introduce(), this.klass.getNumber());
    }

    public void join(Klass klass) {
        if (this.klass != null) {
            this.klass.getStudents().remove(this);
        }
        this.klass = klass;
        this.klass.getStudents().add(this);
    }

    public boolean isIn(Klass klass) {
        if (this.klass == null) {
            return false;
        }
        return klass.getStudents().contains(this);
    }


}
