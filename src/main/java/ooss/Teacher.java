package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class Teacher extends Person implements Observer{


    List<Klass> klasses = new ArrayList<>();

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public void response(Klass klass) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.%n", getName(), klass.getNumber(), klass.getLeader().getName());
    }

    @Override
    public String introduce() {
        return klasses.size() == 0 ? String.format("%s I am a teacher.", super.introduce())
                                   : classIntroduction();
    }

    public String classIntroduction() {
        StringJoiner joiner = new StringJoiner(", ");
        klasses.forEach(klass -> joiner.add(String.valueOf(klass.getNumber())));
        return String.format("%s I am a teacher. I teach Class %s.", super.introduce(), joiner);
    }

    public void assignTo(Klass klass) {
        if (!klasses.contains(klass)) {
            klass.setTeacher(this);
            klasses.add(klass);
        }
    }

    public boolean belongsTo(Klass klass) {
        return klasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return klasses.stream().flatMap(klass -> klass.getStudents().stream()).anyMatch(stu -> stu.equals(student));
    }


}
