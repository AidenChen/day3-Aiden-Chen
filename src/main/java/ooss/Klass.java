package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
@SuppressWarnings("all")
public class Klass extends Subject{

    private final int number;

    private final List<Student> students = new ArrayList<>();

    private Teacher teacher;

    private Student leader;

    public Klass(int number) {
        this.number = number;
    }


    public void assignLeader(Student student) {
        if (!this.getStudents().contains(student)) {
            System.out.println("It is not one of us.");
        }
        this.leader = student;
        super.observers.forEach(observer -> observer.response(this));
    }

    public boolean isLeader(Student student) {
        return student.equals(this.leader);
    }

    public void attach(Observer observer) {
        super.addObserver(observer);
    }


    public int getNumber() {
        return number;
    }
    public List<Student> getStudents() {
        return students;
    }

    public Student getLeader() {
        return leader;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }


}
